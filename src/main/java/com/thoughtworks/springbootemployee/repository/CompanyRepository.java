package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CompanyRepository {
    public List<Company> companyList = new ArrayList<>() {{
        add(new Company(1, "spring"));
        add(new Company(2, "boot"));
        add(new Company(3, "mac"));
        add(new Company(4, "apple"));
        add(new Company(5, "huawei"));
    }};

    public EmployeeRepository employeeRepository = new EmployeeRepository();

    public List<Company> getCompanyList() {
        return companyList;
    }

    public void addCompanyList(Company company) {
        companyList.add(company);
    }

    public Company getCompanyById(int id) {
        return companyList.stream()
                .filter(company -> company.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getAllEmployeesInCompany(int id) {
        return employeeRepository.getEmployeeList().stream()
                .filter(employee -> employee.getCompanyId() == id)
                .collect(Collectors.toList());
    }

    public List<Company> getCompaniesByPageAndSize(int page, int size) {
        return companyList.stream()
                .skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company addCompany(Company company) {
        companyList.add(company);
        return company;
    }

    public Company updateCompany(int id, String name) {
        return companyList.stream()
                .filter(company -> company.getId() == id)
                .findFirst()
                .map(company -> {
                    company.setName(name);
                    return company;
                })
                .orElse(null);
    }

    public int deleteCompany(int id) {
        int companyListSize = companyList.size();
        companyList = companyList.stream().filter(company -> company.getId() != id).collect(Collectors.toList());
        return companyListSize - companyList.size();
    }
}
