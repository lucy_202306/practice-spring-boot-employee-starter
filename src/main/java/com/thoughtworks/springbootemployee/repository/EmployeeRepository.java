package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.db.Data;
import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeRepository {
    public static List<Employee> employeeList = new ArrayList<>() {{
        add(new Employee(1, "zhangsan", 50, "female", 8000, 1));
        add(new Employee(2, "lily", 20, "female", 8000, 1));
        add(new Employee(3, "hana", 22, "male", 10000,2));
        add(new Employee(4, "anna", 25, "male", 6000,2));
    }};

    public static List<Employee> getEmployeeList() {
        return employeeList;
    }

    public Employee getEmployeeById(int id) {
        return employeeList.stream()
                .filter(employee -> employee.getId() == id)
                .findFirst()
                .get();
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return employeeList.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public void addEmployee(Employee employee) {
        employee.setId(getMaxId());
        employeeList.add(employee);
    }

    private int getMaxId() {
        Integer max = employeeList.stream()
                .mapToInt(employee -> employee.getId())
                .max()
                .orElse(0);
        return max + 1;
    }

    public Employee updateEmployeeById(int id, int age, int salary) {
        return employeeList.stream()
                .filter(employee -> employee.getId()==id)
                .findFirst()
                .map(employee -> {
                    employee.setAge(age);
                    employee.setSalary(salary);
                    return employee;
                })
                .orElse(null);
    }

    public int deleteEmployeeById(int id) {
        employeeList = employeeList.stream()
                .filter(employee -> employee.getId() != id)
                .collect(Collectors.toList());
        return id;
    }

    public List<Employee> getEmployeesByPage(int page, int size) {
        return employeeList.stream()
                .skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

}
