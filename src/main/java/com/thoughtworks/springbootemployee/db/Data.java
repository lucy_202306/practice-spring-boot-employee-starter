package com.thoughtworks.springbootemployee.db;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.ArrayList;
import java.util.List;

public class Data {
    public static List<Employee> employeeList = new ArrayList<>() {{
        add(new Employee(1, "zhangsan", 50, "female", 8000, 1));
        add(new Employee(2, "lily", 20, "female", 8000, 1));
        add(new Employee(3, "hana", 22, "male", 10000,2));
        add(new Employee(4, "anna", 25, "male", 6000,2));
    }};

    public static List<Company> companyList = new ArrayList<>() {{
        add(new Company(1, "spring"));
    }};
}
