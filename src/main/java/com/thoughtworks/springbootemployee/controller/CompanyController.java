package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.db.Data;
import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    public CompanyRepository companyRepository = new CompanyRepository();

    @GetMapping
    public List<Company> getAllCompanies() {
        return companyRepository.getCompanyList();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable int id) {
        return companyRepository.getCompanyById(id);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getAllEmployeesInCompany(@PathVariable int id) {
        return companyRepository.getAllEmployeesInCompany(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getCompaniesByPageAndSize(@RequestParam int page, @RequestParam int size) {
        return companyRepository.getCompaniesByPageAndSize(page, size);
    }

    @PostMapping
    public Company addCompany(@RequestBody Company company) {
        return companyRepository.addCompany(company);
    }

    @PutMapping("/{id}")
    public Company updateCompany(@PathVariable int id, @RequestParam String name) {
        return companyRepository.updateCompany(id, name);
    }

    @DeleteMapping("/{id}")
    public int deleteCompany(@PathVariable int id) {
        return companyRepository.deleteCompany(id);
    }
}


