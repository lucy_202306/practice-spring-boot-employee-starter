package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("employees")
public class EmployeeController {
    public EmployeeRepository employeeRepository = new EmployeeRepository();

    @GetMapping
    public List<Employee> getEmployeeList() {
        return employeeRepository.getEmployeeList();
    }

    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable int id) {
        return employeeRepository.getEmployeeById(id);
    }

    @GetMapping(params = "gender")
    public List<Employee> getEmployeeByGender(@RequestParam String gender) {
        return employeeRepository.getEmployeeByGender(gender);
    }

    @PostMapping
    public int addEmployee(@RequestBody Employee employee) {
        employeeRepository.addEmployee(employee);
        return employee.getId();
    }

    @PutMapping("/{id}")
    public Employee updateEmployee(@PathVariable int id, @RequestParam int age, @RequestParam int salary) {
        return employeeRepository.updateEmployeeById(id, age, salary);
    }

    @DeleteMapping("/{id}")
    public int deleteEmployee(@PathVariable int id) {
        return employeeRepository.deleteEmployeeById(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> getEmployeesByPage(@RequestParam int page, @RequestParam int size) {
        return employeeRepository.getEmployeesByPage(page, size);
    }

}
